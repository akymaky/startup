const fetch = require("node-fetch");
const fs = require("fs");
const { execSync } = require("child_process");

global.options = require("./options.json");

global.java = options.types["Minecraft: Java Edition"];
global.bedrock = options.types["Minecraft: Bedrock Edition"];
global.proxy = options.types["Minecraft: Proxy"];

global.download = async (url, path, file) => {
    const res = await fetch(url);
    fs.mkdirSync(path, { recursive: true });
    const fileStream = fs.createWriteStream(path + file);
    return new Promise((resolve, reject) => {
        res.body.pipe(fileStream);
        res.body.on("error", reject);
        fileStream.on("finish", resolve);
    });
};

(async () => {
    const dirs = fs.readdirSync("./modules");
    for (const d of dirs) {
        const files = fs.readdirSync(`./modules/${d}`);
        for (const f of files) {
            try {
                await require(`./modules/${d}/${f}`)();
                fs.writeFileSync(
                    "./options.json",
                    JSON.stringify(options, null, 4)
                );
            } catch (error) {
                console.log(error);
            }
        }
    }
})();
