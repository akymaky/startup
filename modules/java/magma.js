const fetch = require("node-fetch");

module.exports = async () => {
    const res = await fetch(
        "https://api.magmafoundation.org/api/resources/magma/1.12.2/dev/latest"
    );
    const { browser_download_url } = await res.json();

    console.log("Downloading Magma 1.12.2");

    await download(
        browser_download_url,
        "./files/java/magma/1.12.2/",
        "Magma-server.jar"
    );
};
