const cheerio = require("cheerio");
const fetch = require("node-fetch");
const fs = require("fs");
const { execSync } = require("child_process");

module.exports = async () => {
    const res = await fetch("https://files.minecraftforge.net/");
    const html = await res.text();

    let $ = cheerio.load(html);

    const versions = $(
        `ul.nav-collapsible > li > a,
        ul.nav-collapsible > li.elem-active`
    )
        .map(function () {
            return $(this).text();
        })
        .get();

    for (const v of versions) {
        if (v === "1.13.2") break;
        const r = await fetch(
            `https://files.minecraftforge.net/maven/net/minecraftforge/forge/index_${v}.html`
        );
        const h = await r.text();

        $ = cheerio.load(h);

        const data = $(".downloads .download .title")
            .map(function () {
                return $(this).text();
            })
            .get();

        for (let d of data) {
            d = d.split("\n");
            const down = d[1].split(" ").slice(-1)[0].toLowerCase();
            const build = d[2].split(" ").slice(-1)[0];

            const forge = java.software.Forge;

            if (forge.versions[v] && forge.versions[v].build === build) {
                console.log(`Forge ${v} is already up-to-date`);
                continue;
            }

            forge.versions[`${v} - ${down}`] = {
                build,
            };

            console.log(`Building Forge ${v} - ${down} (${build})`);

            const path = `files/${java.path}/${forge.path}/${v}-${down}`;

            if (fs.existsSync(path)) execSync(`rm -r ${path}`);
            fs.mkdirSync(`./${path}`, { recursive: true });

            await download(
                `https://files.minecraftforge.net/maven/net/minecraftforge/forge/${v}-${build}/forge-${v}-${build}-installer.jar`,
                `./${path}/`,
                `forge-installer.jar`
            );

            execSync(`java -jar forge-installer.jar --installServer`, {
                cwd: `./${path}`,
                stdio: "inherit",
            });

            if (fs.existsSync(`./${path}/forge-${v}-${build}.jar`)) {
                fs.renameSync(
                    `./${path}/forge-${v}-${build}.jar`,
                    `./${path}/forge-server.jar`
                );
            }

            if (fs.existsSync(`./${path}/${v}.json`)) {
                fs.unlinkSync(`./${path}/${v}.json`);
            }

            if (`./${path}/forge-installer.jar`) {
                fs.unlinkSync(`./${path}/forge-installer.jar`);
                fs.unlinkSync(`./${path}/forge-installer.jar.log`);
            }
        }
    }
};
