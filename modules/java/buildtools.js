const fetch = require("node-fetch");
const fs = require("fs");
const { execSync } = require("child_process");

module.exports = async () => {
    await download(
        "https://hub.spigotmc.org/jenkins/job/BuildTools/lastSuccessfulBuild/artifact/target/BuildTools.jar",
        "./BuildTools/",
        "BuildTools.jar"
    );

    const res = await fetch(
        "https://hub.spigotmc.org/stash/projects/SPIGOT/repos/builddata/raw/info.json?at=refs%2Fheads%2Fmaster"
    );
    const { minecraftVersion } = await res.json();

    const spigot = java.software.Spigot;
    const craftBukkit = java.software.CraftBukkit;

    if (
        !craftBukkit.versions[minecraftVersion] ||
        !spigot.versions[minecraftVersion]
    ) {
        craftBukkit.versions[minecraftVersion] = {};
        spigot.versions[minecraftVersion] = {};
    }

    console.log(`Building CraftBukkit ${minecraftVersion}`);

    execSync(
        `java -jar BuildTools.jar --rev ${minecraftVersion} --compile craftbukkit`,
        {
            cwd: "./BuildTools",
            stdio: "inherit",
        }
    );

    fs.mkdirSync(
        `./files/${java.path}/${craftBukkit.path}/${minecraftVersion}`,
        {
            recursive: true,
        }
    );

    fs.renameSync(
        `./BuildTools/craftbukkit-${minecraftVersion}.jar`,
        `./files/${java.path}/${craftBukkit.path}/${minecraftVersion}/craftbukkit.jar`
    );

    console.log(`Building Spigot ${minecraftVersion}`);

    execSync(`java -jar BuildTools.jar --rev ${minecraftVersion}`, {
        cwd: "./BuildTools",
        stdio: "inherit",
    });

    fs.mkdirSync(`./files/${java.path}/${spigot.path}/${minecraftVersion}`, {
        recursive: true,
    });

    fs.renameSync(
        `./BuildTools/spigot-${minecraftVersion}.jar`,
        `./files/${java.path}/${spigot.path}/${minecraftVersion}/spigot.jar`
    );
};
