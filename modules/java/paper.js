const fetch = require("node-fetch");

module.exports = async () => {
    const res = await fetch("https://papermc.io/api/v1/paper");
    const json = await res.json();

    const paper = java.software.Paper;

    for (const v of json.versions) {
        const r = await fetch(`https://papermc.io/api/v1/paper/${v}`);
        const j = await r.json();

        if (paper.versions[v] && paper.versions[v].build === j.builds.latest) {
            console.log(`Paper ${v} is already up-to-date`);
            continue;
        }

        paper.versions[v] = {
            build: j.builds.latest,
        };

        console.log(`Downloading Paper ${v} (${j.builds.latest})`);

        await download(
            `https://papermc.io/api/v1/paper/${v}/latest/download`,
            `./files/${java.path}/${paper.path}/${v}/`,
            "paperclip.jar"
        );
    }
};
