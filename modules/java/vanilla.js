const fetch = require("node-fetch");

module.exports = async () => {
    const res = await fetch(
        "https://launchermeta.mojang.com/mc/game/version_manifest.json"
    );
    const json = await res.json();

    for (const v of json.versions) {
        const type = v.type === "release" ? "Vanilla" : "Vanilla Snapshot";
        const vanilla = java.software[type];

        if (!vanilla.versions[v.id]) {
            vanilla.versions[v.id] = {};

            console.log(`Downloading ${type} ${v.id}`);

            await download(
                v.url,
                `./files/${java.path}/${vanilla.path}/${v.id}/`,
                "server.jar"
            );
        }
    }
};
