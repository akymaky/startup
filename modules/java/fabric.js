const fetch = require("node-fetch");
const fs = require("fs");
const { execSync } = require("child_process");

module.exports = async () => {
    const re = await fetch("https://meta.fabricmc.net/v2/versions/installer");
    const jn = await re.json();
    await download(jn[0].url, "./temp/", "fabric-installer.jar");

    const res = await fetch("https://meta.fabricmc.net/v2/versions/game");
    const json = await res.json();

    for (const v of json) {
        const r = await fetch(
            `https://meta.fabricmc.net/v2/versions/loader/${v.version}`
        );
        const j = await r.json();

        if (!v.stable) {
            continue;
        }

        const fabric = java.software.Fabric;

        if (
            fabric.versions[v.version] &&
            fabric.versions[v.version].build === j[0].loader.version
        ) {
            console.log(`Fabric ${v.version} is already up-to-date`);
            continue;
        }

        fabric.versions[v.version] = {
            build: j[0].loader.version,
        };

        await fs.mkdirSync(`./files/java/fabric/${v.version}`, {
            recursive: true,
        });

        console.log(`Building Fabric ${v.version}`);

        await execSync(
            `java -jar temp/fabric-installer.jar server -mcversion ${v.version} -downloadMinecraft -dir files/java/fabric/${v.version}`,
            {
                stdio: "inherit",
            }
        );
    }
};
