const fetch = require("node-fetch");

module.exports = async () => {
    const res = await fetch(
        "https://ci.codemc.io/job/Mohist-Community/job/Mohist-1.12.2/lastSuccessfulBuild/api/json"
    );
    const {
        artifacts: [artifact],
    } = await res.json();

    console.log("Downloading Mohist 1.12.2");

    await download(
        `https://ci.codemc.io/job/Mohist-Community/job/Mohist-1.12.2/lastSuccessfulBuild/artifact/${artifact.relativePath}`,
        "./files/java/mohist/1.12.2/",
        "Mohist-server.jar"
    );
};
