const fetch = require("node-fetch");

module.exports = async () => {
    const res = await fetch("https://update.pmmp.io/api");
    const json = await res.json();

    const { build, base_version, mcpe_version, download_url } = json;

    const pmmp = bedrock.software["PocketMine-MP"];

    if (!pmmp.versions[mcpe_version]) {
        pmmp.versions = {
            [mcpe_version]: {},
            ...pmmp.versions,
        };
    }

    if (pmmp.versions[mcpe_version].build === build) {
        console.log("PocketMine-MP is already up-to-date");
        return;
    }

    pmmp.versions[mcpe_version] = {
        build,
        base_version,
    };

    console.log(
        `Downloading PocketMine-MP ${base_version} (${build}) for Bedrock ${mcpe_version}`
    );

    await download(
        download_url,
        `./files/${bedrock.path}/${pmmp.path}/${mcpe_version}/`,
        "PocketMine-MP.phar"
    );
};
