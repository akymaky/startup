const fetch = require("node-fetch");
const cheerio = require("cheerio");

module.exports = async () => {
    const res = await fetch(
        "https://www.minecraft.net/en-us/download/server/bedrock/"
    );
    const html = await res.text();
    const $ = cheerio.load(html);

    const down = $(`a[data-platform="serverBedrockLinux"]`).attr("href");
    console.log(down);

    let version = down.split("-");
    version = version[version.length - 1].split(".").slice(0, -1).join(".");

    const bedrockServer = bedrock.software["Bedrock Server"];

    if (bedrockServer.versions[version]) {
        console.log("Bedrock Server already up to date!");
        return;
    }

    console.log(`Downloading Bedrock Server ${version}`);

    bedrockServer.versions[version] = {};

    await download(
        down,
        `./files/${bedrock.path}/${bedrockServer.path}/${version}/`,
        "bedrock-server.zip"
    );
};
