const fetch = require("node-fetch");

module.exports = async () => {
    let url =
        "https://ci.nukkitx.com/job/NukkitX/job/Server/job/master/lastSuccessfulBuild/api/json";
    const res = await fetch(url);
    let json = await res.json();

    const cloudburst = bedrock.software["Cloudburst Server"];

    let [version] = Object.keys(cloudburst.versions);

    if (json.changeSets.items) {
        for (const { msg } of json.changeSets.items) {
            if (msg.startsWith("Bedrock") || msg.startsWith("Initial")) {
                const v = json.changeSets.items[0].msg.match(
                    /((\d)+\.)?(\d)+\.(\d)+/g
                );
                version = v || version;
                break;
            }
        }
    }

    const re = await fetch(
        "https://ci.nukkitx.com/job/NukkitX/job/Server/api/json"
    );
    const jn = await re.json();

    for (const job of jn.jobs) {
        if (job.name.startsWith("feature")) {
            const v = job.name.match(/((\d)+\.)?(\d)+\.(\d)+/g);
            if (v && version && !version.startsWith(v)) {
                version = v;
                url = `https://ci.nukkitx.com/job/NukkitX/job/Server/job/${job.name}/lastSuccessfulBuild/api/json`;
                const r = await fetch(url);
                json = await r.json();
            }
        }
    }

    if (!cloudburst.versions[version]) {
        cloudburst.versions = {
            [version]: {},
            ...cloudburst.versions,
        };
    }

    if (cloudburst.versions[version].build === json.number) {
        console.log("Cloudburst Server is already up-to-date");
        return;
    }

    cloudburst.versions[version] = {
        build: json.number,
    };

    console.log(`Downloading Cloudburst Server ${version} (${json.number})`);

    await download(
        `${url}/artifact/${json.artifacts[0].relativePath}`,
        `./files/${bedrock.path}/${cloudburst.path}/${version}/`,
        "Nukkit.jar"
    );
};
