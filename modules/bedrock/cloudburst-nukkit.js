const fetch = require("node-fetch");

module.exports = async () => {
    let url =
        "https://ci.nukkitx.com/job/NukkitX/job/Nukkit/job/master/lastSuccessfulBuild/api/json";
    const res = await fetch(url);
    let json = await res.json();

    const nukkit = bedrock.software["Cloudburst Nukkit"];

    let [version] = Object.keys(nukkit.versions);

    if (json.changeSets.items) {
        for (const { msg } of json.changeSets.items) {
            if (msg.startsWith("Bedrock") || msg.startsWith("Initial")) {
                const v = json.changeSets.items[0].msg.match(
                    /((\d)+\.)?(\d)+\.(\d)+/g
                );
                version = v || version;
                break;
            }
        }
    }

    const re = await fetch(
        "https://ci.nukkitx.com/job/NukkitX/job/Nukkit/api/json"
    );
    const jn = await re.json();

    for (const job of jn.jobs) {
        if (job.name.startsWith("feature")) {
            const v = job.name.match(/((\d)+\.)?(\d)+\.(\d)+/g);
            if (v && version && !version.startsWith(v)) {
                version = v;
                url = `https://ci.nukkitx.com/job/NukkitX/job/Nukkit/job/${job.name}/lastSuccessfulBuild/api/json`;
                const r = await fetch(url);
                json = await r.json();
            }
        }
    }

    if (!nukkit.versions[version]) {
        nukkit.versions = {
            [version]: {},
            ...nukkit.versions,
        };
    }

    if (nukkit.versions[version].build === json.number) {
        console.log("Cloudburst Nukkit is already up-to-date");
        return;
    }

    nukkit.versions[version] = {
        build: json.number,
    };

    console.log(`Downloading Cloudburst Nukkit ${version} (${json.number})`);

    await download(
        `${url}/artifact/${json.artifacts[0].relativePath}`,
        `./files/${bedrock.path}/${nukkit.path}/${version}/`,
        "nukkit.jar"
    );
};
