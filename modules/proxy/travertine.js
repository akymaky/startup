const fetch = require("node-fetch");

module.exports = async () => {
    const res = await fetch("https://papermc.io/api/v1/travertine");
    const json = await res.json();

    const travertine = proxy.software.Travertine;

    const r = await fetch(
        `https://papermc.io/api/v1/travertine/${json.versions[0]}`
    );
    const j = await r.json();

    if (
        travertine.versions.latest &&
        travertine.versions.latest.build_id === j.builds.latest
    ) {
        console.log("Travertine is alreadt up-to-date");
        return;
    }

    console.log(`Downloading Travertine (${j.builds.latest})`);

    travertine.versions.latest = {
        build: j.builds.latest,
    };

    await download(
        `https://papermc.io/api/v1/travertine/${json.versions[0]}/latest/download`,
        `./files/${proxy.path}/${travertine.path}/latest/`,
        "Travertine.jar"
    );
};
