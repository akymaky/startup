const fetch = require("node-fetch");

module.exports = async () => {
    const url =
        "https://ci.codemc.io/job/yesdog/job/Waterdog/lastSuccessfulBuild/";
    const res = await fetch(`${url}/api/json`);
    const json = await res.json();

    const waterdog = proxy.software.Waterdog;

    if (waterdog.versions.latest.build === json.number) {
        console.log("Waterdog is already up-to-date");
        return;
    }

    waterdog.versions.latest = {
        build: json.number,
    };

    console.log(`Downloading Waterdog (${json.number})`);

    await download(
        `${url}/artifact/${json.artifacts[0].relativePath}/`,
        `./files/${proxy.path}/${waterdog.path}/latest/`,
        "Waterdog.jar"
    );
};
