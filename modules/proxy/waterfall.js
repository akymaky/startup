const fetch = require("node-fetch");

module.exports = async () => {
    const res = await fetch("https://papermc.io/api/v1/waterfall");
    const json = await res.json();

    const waterfall = proxy.software.Waterfall;

    const r = await fetch(
        `https://papermc.io/api/v1/waterfall/${json.versions[0]}`
    );
    const j = await r.json();

    if (
        waterfall.versions.latest &&
        waterfall.versions.latest.build_id === j.builds.latest
    ) {
        console.log("Waterfall is already up-to-date");
        return;
    }

    console.log(`Downloading Waterfall (${j.builds.latest})`);

    waterfall.versions.latest = {
        build: j.builds.latest,
    };

    await download(
        `https://papermc.io/api/v1/waterfall/${json.versions[0]}/latest/download`,
        `./files/${proxy.path}/${waterfall.path}/latest/`,
        "Waterfall.jar"
    );
};
