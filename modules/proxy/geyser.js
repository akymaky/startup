const fetch = require("node-fetch");

module.exports = async () => {
    const url =
        "https://ci.nukkitx.com/job/GeyserMC/job/Geyser/job/master/lastSuccessfulBuild";
    const res = await fetch(`${url}/api/json`);
    const json = await res.json();

    const geyser = proxy.software.Geyser;

    if (geyser.versions.latest.build === json.number) {
        console.log("Geyser is already up-to-date");
        return;
    }

    geyser.versions.latest = {
        build: json.number,
    };

    console.log(`Downloading Geyser (${json.number})`);

    await download(
        `${url}/artifact/${json.artifacts[3].relativePath}/`,
        `./files/${proxy.path}/${geyser.path}/latest/`,
        "Geyser.jar"
    );
};
