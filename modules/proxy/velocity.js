const fetch = require("node-fetch");

module.exports = async () => {
    const url = "https://ci.velocitypowered.com/job/velocity";
    const res = await fetch(`${url}/api/json`);
    const json = await res.json();

    const velocity = proxy.software.Velocity;

    if (velocity.versions.latest.build === json.number) {
        console.log("Velocity is already up-to-date");
        return;
    }

    velocity.versions.latest = {
        build: json.number,
    };

    console.log(`Downloading Velocity (${json.number})`);

    await download(
        `${url}/artifact/${json.artifacts[0].relativePath}/`,
        `./files/${proxy.path}/${velocity.path}/latest/`,
        "velocity-proxy.jar"
    );
};
