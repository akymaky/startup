const fetch = require("node-fetch");

module.exports = async () => {
    const url = "https://ci.md-5.net/job/BungeeCord/lastSuccessfulBuild";
    const res = await fetch(`${url}/api/json`);
    const json = await res.json();

    const bungeeCord = proxy.software.BungeeCord;

    if (bungeeCord.versions.latest.build === json.number) {
        console.log("BungeeCord is already up-to-date");
        return;
    }

    bungeeCord.versions.latest = {
        build: json.number,
    };

    console.log(`Downloading BungeeCord (${json.number})`);

    await download(
        `${url}/artifact/${json.artifacts[0].relativePath}/`,
        `./files/${proxy.path}/${bungeeCord.path}/latest/`,
        "BungeeCord.jar"
    );
};
